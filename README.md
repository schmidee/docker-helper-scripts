# Docker-Helper-Scripts

Scripts to help with docker startup and shut down.

## Docker Start Up ([dockerStartUp.sh](dockerStartUp.sh))
This assists with starting up docker instances for most projects. This will 
always perform `docker-compose up -d` regardless of what parameters are provided
If no parameters are provided, it will open up a bash window to the php-fpm docker 
container which has composer installed on it.

Please note that you can only use one option at a time. **Also you must run this in
the root directory of your project.**

### Arguments
*  **install**: This will run the composer installation for the project.
*  **PHP Version**: This will specify the version of PHP you want to use. It will default to 7.3. 

### Examples Commands
Run Composer Install using PHP 8.0 
```
startdoc install 8.0

```
Start up using PHP version 8.0

```
startdoc 8.0

```

## Docker Stop ([dockerStop.sh](dockerStop.sh))
This assists with stoping all docker instances. Essecially its the same as running
`docker-compose down`.

## Docker PHPUnit ([dockerPHPUnit.sh](dockerPHPUnit.sh))
This script just opens a bash shell to a instance with PHPUnit ready to go. This 
is a script I made awhile ago when PHPUnit had issues running. I am really no longer 
using this script, but its here in case the need arises.

### Arguments
*  **PHP Version**: This will specify the version of PHP you want to use. It will default to 7.3. 

## Check In Helper ([checkIn.sh](checkIn.sh))
This script assist with prepping your code for check into Git. It currently automatically 
runs CS Fixer on the app, tests, and database directories. This will automatically apply 
the changes so keep that in mind when running. 

In addition to running CS Fixer it will run the Code Coverage metrics so they can be 
reviewed prior to check in to make sure your coverage is up to par in addition to running 
the dependancy update check. Dependancy upgrades will not be applied automatically.

### Arguments 
*  **PHP Version**: This will specify the version of PHP you want to use. It will default to 7.3. 

## Bash Alias Setup
1.  In your home directory, create a .bash_profile file. If one already exists just edit it.
2.  At the top of the file do the following:
```
alias startdoc='source ~/dockerStartUp.sh'
alias stopdoc='~/dockerStop.sh'
alias pudoc='~/dockerPHPUnit.sh'
alias ci='~/checkIn.sh'
```
 * Keep in mind you need to use the path to where you put your copies of the 
scripts. Mine are just in my home directory. You can also give them different
aliases as well if you like.
3. Save the file
4. Restart your terminal
5. Test out your new commands!
    * You should just be able to type "startdoc" and it will kick off the script.

