#!/bin/sh
version="${2:-7.3}"
docker run -it --rm -v $(pwd):/src -v ~/.ssh:/root/.ssh registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-phpunit bash
