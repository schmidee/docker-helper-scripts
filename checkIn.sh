 #!/bin/sh
 version="${2:-7.3}"
 echo "Using PHP Version: $version";
 echo "******** Running PHP CS Fixer ********"
 docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-php-cs-fixer -a app/
 docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-php-cs-fixer -a tests/
 docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-php-cs-fixer -a database/
 echo "******** Running Coverage Metrics ********"
 docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-phpunit-coverage
 echo "******** Checking Dependancies for Updates ********"
 docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:dependency-check check:outdated-dependencies
