#!/bin/sh

if [[ $# -ne 0 && $1 = "install" ]]; then
	version="${2:-7.3}"
else
	version="${1:-7.3}"		
fi

echo "Using PHP Version: $version";

if [ $# -ne 0 ]; then
    if [ $1 = "install" ]; then
    	echo "Running Composer Install"
    	docker run -it --rm -v $(pwd):/opt/project -v ~/.ssh:/root/.ssh registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-devtools composer install
    fi		
fi

echo "Starting up Docker"
docker-compose up -d
echo "Starting up Bash Console"
docker run -it --rm -v $(pwd):/opt/project -v ~/.ssh:/root/.ssh registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:$version-devtools bash

		

